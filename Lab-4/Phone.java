public class Phone{
	private double price;
	private String brand;
	private String condition;
	public void finalPrice(){
		if(this.condition.equals("used"))this.price/=2;
	}
	public double getPrice(){
		return this.price;
	}
	public String getBrand(){
		return this.brand;
	}
	public String getCondition(){
		return this.condition;
	}
	public void setPrice(double input){
		this.price=input;
	}
	public void setCondition(String input){
		this.condition=input;
	}
	public Phone(double price,String brand,String condition){
		this.price=price;
		this.brand=brand;
		this.condition=condition;
	}
}