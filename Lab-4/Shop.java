import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Phone[] products = new Phone[4];
		for(int i=0;i<products.length;i++){
			System.out.println("Enter price:\nEnter brand:\nEnter condition:");
			products[i]=new Phone(Double.parseDouble(reader.nextLine()),reader.nextLine(),reader.nextLine());
		}
		System.out.println("Specs of the last product before changing them: "+products[3].getPrice() +" "+ products[3].getBrand() +" "+ products[3].getCondition());
		System.out.println("Enter new price for the last one");
		products[3].setPrice(Double.parseDouble(reader.nextLine()));
		System.out.println("Enter new condition for the last one");
		products[3].setCondition(reader.nextLine());
		System.out.println("Specs of the last product after changing them: "+products[3].getPrice() +" "+ products[3].getBrand() +" "+ products[3].getCondition());
		System.out.println(products[3].getPrice() +" "+ products[3].getBrand() +" "+ products[3].getCondition());
		System.out.println("Initial price of the last product: "+ products[3].getPrice());
		products[3].finalPrice();
		System.out.println("Final price of the last product after taking in consideration its condition: "+ products[3].getPrice());
	}
}